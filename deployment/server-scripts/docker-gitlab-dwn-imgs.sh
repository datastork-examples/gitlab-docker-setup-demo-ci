#!/bin/bash

ACCESS_TOKENS=~/.access_tokens
GITLAB_REGISTRY=registry.gitlab.com/datastork-examples/gitlab-docker-setup-demo-ci

. "${ACCESS_TOKENS}"

if [ -z "${GITLAB_REGISTRY_USER}" ]; then
  echo "Please specify GITLAB_REGISTRY_USER in ${ACCESS_TOKENS}" 1>&2
  exit 1
fi

if [ -z "${GITLAB_REGISTRY_TOKEN}" ]; then
  echo "Please specify GITLAB_REGISTRY_TOKEN in ${ACCESS_TOKENS}" 1>&2
  exit 1
fi

TAG=${1}
if [[ -z "${TAG}" ]]; then
  echo "Scripts expects to pass the docker image tag to download" 1>&2
  exit 1
fi

echo -e "\nDownloading images for tag: ${TAG}"

docker login -u "${GITLAB_REGISTRY_USER}" -p "${GITLAB_REGISTRY_TOKEN}" registry.gitlab.com

IMAGES="hello-world"

for i in ${IMAGES}; do
  RMT_IMG="${GITLAB_REGISTRY}/${i}:${TAG}"
  echo -e "\nDownloading ${RMT_IMG}"
  if docker pull "${RMT_IMG}"; then
    docker tag "${RMT_IMG}" "${i}:release"
  else
    echo -e "\nFailed to download image: ${RMT_IMG}" 1>&2
    exit 1
  fi
done
