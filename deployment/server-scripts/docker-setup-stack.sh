#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

cd "${DIR}"

docker network create --driver=overlay --attachable helloworld-net >/dev/null 2>&1

docker stack up "--compose-file=${DIR}/docker-compose.yml" "helloworld"

docker service ls
