#!/bin/bash

CRON_CMD="/opt/helloworld/cron/do-new-deployment.sh"
CRON_JOB="*/1 * * * * ${CRON_CMD}"

(
  crontab -l | grep -v -F "${CRON_CMD}"
  echo -e "${CRON_JOB}"
) | crontab -

echo -e "\nInstalled new cron job:"
crontab -l
