#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

SRV_USER=srvusr
GITLAB_USER=gitlabusr

RELEASES="/home/${GITLAB_USER}/releases"
BACKEND_RELEASES="${RELEASES}/backend"

TIME_NOW=$(date '+%Y%m%d-%H%M%S')

TMP_DIR=$(mktemp -d -t helloworld-deployment-XXXXXXXXXX)

if [ ! -f "${BACKEND_RELEASES}/docker-tag" ]; then
  exit 0
fi

LOG_DIR="/var/log/helloworld/deployment/${TIME_NOW:0:4}/${TIME_NOW:4:2}"
mkdir -p "${LOG_DIR}"

LOG_FILE="${LOG_DIR}/deployment-${TIME_NOW}.log"

echo -e "\nRedirect output to log\n${LOG_FILE}"
exec 1<&-
exec 2<&-
exec 1<>"${LOG_FILE}"
exec 2>&1

mkdir -p "${LANDING_PAGE_DST}"

echo -e "\nStarting new deployment at ${TIME_NOW}"

sleep 10

if [ -f "${BACKEND_RELEASES}/docker-tag" ]; then

  echo -e "\nStop API service"

  docker service scale helloworld_hello-world=0

  sleep 10

  USER_EMAIL=$(cat ${BACKEND_RELEASES}/executor-user-email)
  rm -f ${BACKEND_RELEASES}/executor-user-email

  DOCKER_TAG=$(cat "${BACKEND_RELEASES}/docker-tag")
  rm -f "${BACKEND_RELEASES}/docker-tag"

  echo -e "\nDeploying backend docker tag: ${DOCKER_TAG}\non behalve of user: ${USER_EMAIL}"

  /opt/helloworld/docker-gitlab-dwn-imgs.sh "${DOCKER_TAG}"

  /opt/helloworld/docker-setup-stack.sh
fi

echo -e "\nStart API service"
docker service scale helloworld_hello-world=1

echo -e "\nWait services to start..."
sleep 30

echo -e "\nList service containers"
docker ps --all

echo -e "\nCleanup ${TMP_DIR}"
rm -rf "${TMP_DIR}"

echo -e "\nCleanup old docker images"
docker volume ls -qf dangling=true | xargs -r docker volume rm
docker image prune --force -a --filter "until=48h"
