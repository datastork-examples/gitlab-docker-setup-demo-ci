#!/bin/bash

function set_secret {
  echo -n "Enter secret ${1}:"
  read -s SECRET

  echo -e "\nStoring secret: ${SECRET:0:2}***${SECRET:${#SECRET}-2:2}"

  docker secret rm ${1} > /dev/null 2>&1

  echo "${SECRET}" | docker secret create ${1} -

  echo
}

#set_secret sample-key

docker secret ls
