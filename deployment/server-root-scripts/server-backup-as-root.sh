#!/bin/bash

BACKUP_DIR=/opt/backups
mkdir -p "${BACKUP_DIR}"

BACKUP_FILE="${BACKUP_DIR}/helloworld-backup-$(date '+%Y%m%d-%H%M%S').tgz"

echo -e "\nCreating backup: ${BACKUP_FILE}"
tar --exclude='/opt/helloworld/jenkins-home/workspace' \
  --exclude='.m2' \
  --exclude='/home/srvusr/tmp' \
  -czvf "${BACKUP_FILE}" \
  /opt/HelloWorld \
  /home/gitlabusr \
  /home/srvusr
