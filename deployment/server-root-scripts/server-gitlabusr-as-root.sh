#!/bin/bash

GITLAB_USER=gitlabusr
SRV_USER=srvusr

GTILAB_PRIV_KEY_DIR=/home/${SRV_USER}/.ssh/${GITLAB_USER}

adduser "${GITLAB_USER}" --gecos "GitLab User,,," --disabled-password

mkdir /home/"${GITLAB_USER}"/.ssh
touch /home/"${GITLAB_USER}"/.ssh/authorized_keys

mkdir -p /home/"${GITLAB_USER}"/releases/backend
mkdir -p /home/"${GITLAB_USER}"/releases/frontend
mkdir -p /home/"${GITLAB_USER}"/releases/landing-page

chmod o+rwx /home/"${GITLAB_USER}"/releases/backend
chmod o+rwx /home/"${GITLAB_USER}"/releases/frontend
chmod o+rwx /home/"${GITLAB_USER}"/releases/landing-page

chown -R "${GITLAB_USER}:${GITLAB_USER}" /home/"${GITLAB_USER}"

mkdir -p "${GTILAB_PRIV_KEY_DIR}"

ssh-keygen -C "${GITLAB_USER}" \
  -f "${GTILAB_PRIV_KEY_DIR}/id_rsa_${GITLAB_USER}" -P ''

chown -R "${SRV_USER}:${SRV_USER}" "${GTILAB_PRIV_KEY_DIR}"

cat "${GTILAB_PRIV_KEY_DIR}/id_rsa_${GITLAB_USER}.pub" >>/home/"${GITLAB_USER}"/.ssh/authorized_keys

echo -e "\nUser ${GITLAB_USER} created.\nTo have access please add your SSH\
 public keys to\n/home/"${GITLAB_USER}"/.ssh/authorized_keys"
