#!/bin/bash

SRV_USER=srvusr

echo -e "\nUpdating OS and installing required packages"

apt-get update

apt-get install -y \
  curl htop vim jq apache2-utils unzip python-pip \
  apt-transport-https ca-certificates \
  software-properties-common


#apt-get install -y docker-ce

# Ubuntu 18.04
apt-get install -y docker.io

pip install docker-compose

systemctl disable lighttpd
systemctl disable apache2
systemctl disable mysql

adduser "${SRV_USER}" --gecos "User,,," --disabled-password

mkdir -p "/home/${SRV_USER}/.ssh"
chmod 700 "/home/${SRV_USER}/.ssh"

touch "/home/${SRV_USER}/.ssh/authorized_keys"

chown -R "${SRV_USER}:${SRV_USER}" /home/"${SRV_USER}"

echo -e "\nUser ${SRV_USER} created.\nTo have access please add your SSH\
 public keys to\n/home/"${SRV_USER}"/.ssh/authorized_keys"

usermod -aG docker "${SRV_USER}"

DIRS="/var/log/helloworld /opt/helloworld"

for i in ${DIRS}; do
  echo "Create ${i}"
  mkdir "${i}"
  chown "${SRV_USER}":"${SRV_USER}" "${i}"
done

# Restrict other users to not have accesss to the deployment
chmod o-rwx /opt/helloworld

echo -e "\nUser ${SRV_USER} added to docker group, please reboot to take effect."
