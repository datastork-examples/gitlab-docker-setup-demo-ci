#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

ROOT_SERVER_USER_HOST=root@helloworld-demo
SRVUSR_SERVER_USER_HOST=srvusr@helloworld-demo

SSH_OPT="-o ConnectTimeout=10 -o LogLevel=quiet \
 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

function execute_ssh_cmd_root() {
  ssh ${SSH_OPT} "${ROOT_SERVER_USER_HOST}" "${1}"
}

echo -e "\nCopy root scripts"
execute_ssh_cmd_root "mkdir -p /opt/helloworld-root-scripts/"
scp -rp ${SSH_OPT} "${DIR}/../server-root-scripts"/* "${ROOT_SERVER_USER_HOST}:/opt/helloworld-root-scripts/"

echo -e "\nCopy server scripts"
scp -rp ${SSH_OPT} "${DIR}/../server-scripts"/* "${SRVUSR_SERVER_USER_HOST}:/opt/helloworld/"
scp -rp ${SSH_OPT} "${DIR}/../server-demo"/* "${SRVUSR_SERVER_USER_HOST}:/opt/helloworld/"
