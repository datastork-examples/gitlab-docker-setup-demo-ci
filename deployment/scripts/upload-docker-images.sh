#!/bin/bash

command -v pv >/dev/null 2>&1 ||
  {
    echo -e "This script need pv, please install it:\n\nsudo apt-get install pv" >&2
    exit 1
  }

SSH_USER_HOST=${1}
IMAGES_LIST=${2}

function dump_usage() {
  echo -e "Example usage:\n\n$(basename ${0}) user@server-host 'image1:tag1 image2:tag2 image3:tag3'"
}

if [[ -z "${SSH_USER_HOST}" ]]; then
  echo "Please specify ssh user@host" 1>&2
  dump_usage
  exit 2
fi

if [[ -z "${IMAGES_LIST}" ]]; then
  echo "Please specify list of docker images" 1>&2
  dump_usage
  exit 3
fi

for i in ${IMAGES_LIST}; do
  imgSize=$(docker image inspect "${i}" --format='{{.Size}}')

  if [[ $? != 0 ]]; then
    echo -e "Image does not exists: ${i}" 1>&2
    continue
  fi

  imgSize=$((imgSize / 1024 / 1024))
  echo -e "\nUploading image ${imgSize} MB - ${i}"

  SECONDS=0

  docker save ${i} | pv | ssh ${SSH_USER_HOST} 'docker load'

  eval "echo $(date -ud "@$SECONDS" +'$((%s/3600/24)) days %H hours %M minutes %S seconds')"

  m=$((${SECONDS} / 60))
  s=$((${SECONDS} % 60))
  echo -e "Uploaded for: ${m}m ${s}s\n"

done
