#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

docker-compose -p hello-world -f ${DIR}/target/docker-compose.yml $*
