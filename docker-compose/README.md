# HelloWorldDocker Compose

This will generate docker compose files for the current maven project version and git commit tag.

To start a local test environment execute:

```bash
./docker-compose/docker-compose.sh up -d
```

The above will run HelloWorldAPI docker container - spring boot application for the REST API

Check the REST API Swagger:
http://localhost:18080/api/swagger-ui.html

> Note that if you commit to git the tag will change so you should rebuild the full project again in order the docker tag images to be properly updated.

