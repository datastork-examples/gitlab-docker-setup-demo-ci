![Alt text](images/coffee.jpg?raw=true "coffee")

The Continuous Integration (CI) is a permanent part in the process of modern software creation and management. Some developers even don’t know what they use, but still do it. In short, Continuous Integration (CI) is a good practice where developers  integrate their code into a shared repository at least once a day. The changes are validated by auto build and run automated tests, allowing teams to find problems early before merging and deployment. This is especially important when we containerize our app/microservice. Good code practices like CI will save us the last second integration mess and some problems - the container debugging is more difficult and slower for example.

*Many teams find that this approach leads to significantly reduced integration problems and allows a team to develop cohesive software more rapidly.* \
*Martin Fowler*

Here we will show an example project with Java and Spring, module separation, documentation and local testing with Swagger, Docker containerization and CI with tests in GitLab

##### Structure
Let’s start with the main project structure. We will have 4 main modules:
- **application** - here will be separated the main Application.java class + Dockerfile configuration
- **services** - the REST controllers, business logic, DB connection, models and entities plus the corresponding test should be here
- **docker-compose** - the name is self explanatory-the place for docker-compose settings - used for local testing and integration tests
- **utils** - this will be a folder with some helpful bash scripts for the project and the CI

The benefits of using modular structure are many and we won’t stop here to discuss them. It’s enough to say that modules provide distinction of functionality, encapsulation of internal logic, reusability, diversive testing, better dependency management.

The application we will use for the demo is a simple “HelloWorld” REST project. The main parts are a controller to call a few requests, DAO object with some simple print logic and application starter. We’ll separate the “business” login in service module, but  Application class will be located as a separate module in:
``/application/src/main/java/io/datastork/helloworld/Application.java``
This makes the service layer independent and another starter could run with the main code body as well. In ``/services/src/test/java/io/datastork/helloworld/`` we will add TestApp.java starter file and a folder rest with rest/HelloWorldControllerTest.java and rest/BaseTest.java with restTemplate and MVC mock settings for any other Controller tests. 

##### Dependencies and plugins list:

We will use one of the most popular Java dependency management tools - **Maven**. Here we will list only the .pom files with dependencies and plugins related to the article topic.
**Main pom.xml:**
**Plugins:**
*git-commit-id-plugin* Gives basic information of the git commits. We will use it get the specific commit and branch  details building the CI pipeline jobs \
*io.fabric8 docker-maven-plugin* This is a plugin for building and managing Docker images and containers. It communicates with the Docker daemon and has a lot of functionality.
More information could be found in the Git repo: https://github.com/fabric8io/docker-maven-plugin

Docker is used for building, shipping, and running distributed applications. It uses a loosely isolated environment, i.e packs our app and separates it from the infrastructure. The containers contain all that is needed to run the app on any environment supporting docker. The technology is widely used in the cloud-based "world", too. 
More about **Docker** : https://docs.docker.com/engine/docker-overview/

Specified here are: the plugin version, the goal we want to achieve (create and start containers, stop and destroy them, build images, push or remove images, show logs, create or remove volumes and so on), corresponding execution id
```xml
<plugin>
 <groupId>io.fabric8</groupId>
 <artifactId>docker-maven-plugin</artifactId>
 <version>${docker.maven.version}</version>
 <executions>
   <execution>
     <id>docker-build</id>
     <goals>
       <goal>build</goal>
     </goals>
   </execution>
   <execution>
     <id>docker-push</id>
     <phase>deploy</phase>
     <goals>
       <goal>push</goal>
     </goals>
   </execution>
 </executions>
</plugin>

```

**application/pom.xml:**
**Plugins**: \
*git-commit-id-plugin* \
*docker-maven-plugin* \
The child pom uses the setting from the parent, but here we will make the additional settings for the docker plugin like alias, name, tag, dockerFileDir path and the path to the jar used  for image creation

```xml
<plugin>
     <groupId>io.fabric8</groupId>
     <artifactId>docker-maven-plugin</artifactId>
     <configuration>
       <images>
         <image>
           <alias>hello-world</alias>
           <name>hello-world:${project.version}-${git.commit.id.abbrev}</name>
           <build>
             <tags>
               <tag>latest</tag>
             </tags>
             <dockerFileDir>${project.basedir}/src/main/docker</dockerFileDir>
             <assembly>
               <inline>
                 <files>
                   <file>
                     <source>${project.build.directory}/application-${project.version}.jar</source>
                   </file>
                 </files>
               </inline>
             </assembly>
           </build>
         </image>
       </images>
     </configuration>
   </plugin>
```

#### Swagger
Swagger is a popular tool with a lot of capabilities. The essentials are a document, usually json or yaml with specifications describing REST APIs. The format is both machine-readable and human-readable. As a result, it can be used to create and share documentation among product managers, testers and developers, to automate API-related processes or to generate a whole REST API from the Swagger doc only as source. 
We will add some configurations  in config directory ``/services/src/main/java/io/datastork/helloworld/config/SwaggerConfig.java``,  and in ``/services/src/main/resources/application.properties``
Some definitions are taken from the git plugin and set here: like name, version and so on. Maven-resource-plugin will deal with them. The git commit version and the other fields are injected into the SwaggerConfig.java and could be found in the UI: ``http://localhost:18080/api/swagger-ui.html``
![Alt text](images/swagger.jpg?raw=true "coffee")

#### Dockerfile
The maven docker plugin reads the common format for building docker images -dockerfile.It contains all the commands a user could call on the command line to assemble an image.  We will make ours in:

``/application/src/main/docker/Dockerfile``
```shell script
FROM openjdk:8

RUN apt-get update

ENV WORK_DIR=/opt/hello-world

RUN mkdir -p ${WORK_DIR}

COPY maven/application-${project.version}.jar ${WORK_DIR}/app.jar

COPY *.sh ${WORK_DIR}/
WORKDIR ${WORK_DIR}/

ENTRYPOINT bash ${WORK_DIR}/helloworld-starter.sh
```

In short, here we create a work directory, copy a helper shell script and start it with ``ENTRYPOINT`` The command configures a container and runs it as executable. And the script itself just goes to the source dir and runs the jar with some options

```shell script
#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar "${DIR}/app.jar"
```
Note: It is important to tag the docker file in resources ``/application/src/main/resources/docker-tag``

```
@project.version@-@git.commit.id.abbrev@
```

The application could be built now. If we go to the root dir and run mvn clean install in terminal, the maven logs will show:
- any previous build results are cleaned
- the application compiles
- (if any)unit tests are executed
- git info taken, resources copied, the .jar file is built
- maven docker plugin builds .tar and “hello-world” image

##### Docker-compose
We don’t really need Docker-compose with this “HelloWorld” or any simple app, but it’s power comes to hand for defining and running multi-container Docker applications.
As Docker’s docs says, here we use have to use a YAML file to configure the services, create and start all of them with a single command.
Compose consists of  3 main steps:
- environment settings with ``Dockerfile``
- service definition in ``docker-compose.yml``
- executing ``docker-compose up`` to start and run all the containers.
Our file will be in ``/docker-compose/src/main/docker/docker-compose.yml``. Here we set image name, JAVA_OPTS like JVM memory arguments, JMX monitoring settings, debugging with JDWP and ports where we want to run and debug our app.
Let’s start compose with another bash scrip:
*docker-compose/docker-compose.sh*

##### GitLab CI:
**Prerequisites**: We need a GitLab account and repo with the project (What a surprise!). The account should have at least Maintainer privileges for this repo and some minutes for running GitLab pipeline jobs

The whole “magic” has its foundations by a single file .gitlab-ci.yml  at the repository’s root dir. The scripts set in this file are executed by internal tool called GitLab Runner.

The documents on GitLab site are a good start and explain how to do the job: \
https://docs.gitlab.com/ee/ci/introduction/index.html#how-gitlab-cicd-works \
https://docs.gitlab.com/ee/ci/introduction/index.html#basic-cicd-workflow \
https://docs.gitlab.com/ee/user/project/pages/getting_started_part_four.html

The .yaml file consists of organized scripts into jobs, and the jobs form a pipeline.
Names of jobs, scripts and operations in most cases are self-explanatory but here we will explain each section of this file

```
image: maven:latest

stages:
 - build
 - test
 - package
 - master-build-image
 - manual-build-image
 - deploy-to-prod
 - deploy-to-dev
```

There are three default stages on GitLab CI: build, test, and deploy. Here we define some additional stages for better tuning and of course testing before deployment
```yaml
variables:
 MAVEN_CLI_OPTS: "-s $CI_PROJECT_DIR/.m2/settings.xml --batch-mode"
 MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
 DOCKER_REGISTRY_PATH: "registry.gitlab.com/datastork-examples/gitlab-docker-setup-demo-ci"
 DOCKER_TAG: "${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHORT_SHA}"
```
Some variables are declared. DOCKER_REGISTRY_PATH and DOCKER_TAG could be found here on the repo’s page under container_registry
https://gitlab.com/datastork-examples/gitlab-docker-setup-demo-ci/container_registry

```yaml
cache:
 paths:
   - .m2/repository
```
The upper rows purpose is to cache the installation files for dependencies and achieve a faster build.

```yaml
build:
 stage: build
 script:
   - mvn $MAVEN_CLI_OPTS compile
```
The build job. We compile the project with mvn compile script here.

```yaml
test:
 stage: test
 script:
   - mvn $MAVEN_CLI_OPTS test
 artifacts:
   when: always
   reports:
     junit: "**/target/surefire-reports/TEST-*.xml"
```
The test job on test stage. We execute the project’s test with mvn test script here.
*Note*: Artifacts definition by the documentation: Job artifacts are a list of files and directories created by a job once it finishes.
This feature is enabled by default in all GitLab installations. The full list from which we can attach on success be found on the corresponding README https://docs.gitlab.com/ee/ci/yaml/README.html#artifacts
In our case, we define that surefire maven plugin always should generate report to the given file ``TEST-*.xml``

```yaml
package:
 stage: package
 script:
   - mvn $MAVEN_CLI_OPTS package -DskipTests
 artifacts:
   name: "hello-world-${DOCKER_TAG}"
   when: on_success
   expire_in: 15 days
   paths:
     - "**/target/*.jar"
```
This job packs the application into .jar with proper name skipping the already executed test or faster build speed.
The result artifact (.jar) is created on success only and have expiration period

```yaml
master-build-image:
 stage: master-build-image
 image: snirnx/docker-maven:3.5.4
 services:
   - docker:dind
 script:
   - $CI_PROJECT_DIR/utils/ci-cd/push-docker-images.sh ${DOCKER_TAG}
 only:
   - master
```
This is a docker job  build job. The base image used is taken from snirnx/docker-maven.
*Note*: The services keyword defines a Docker image that runs during a job linked to the Docker image that the image keyword defines.
This allows access to the service image during build time (Docker image here). It’s manually executed ONLY for the master branch.

```yaml
manual-build-image:
 stage: manual-build-image
 image: snirnx/docker-maven:3.5.4
 services:
   - docker:dind
 script:
   - $CI_PROJECT_DIR/utils/ci-cd/push-docker-images.sh ${DOCKER_TAG}
 when:
   manual
 except:
   - master
```
Another docker related job, almost like the previous one. The only difference is that here we will execute in on every branch EXCEPT the master

The last jobs are identical. The reason we have two instead of single one is simple. The master branch requires different access level on GitLab (Maintainer or Owner).
This means any master related merges and build could be done by selected and usually more experienced members of the dev team. The risk of accidentally publishing bugs is lowered. We strongly encourage pull/merge request usage as well

The last 2 jobs use another shell script placed in ``/utils`` module:
``utils/ci-cd/push-docker-images.sh``
```shell script
#!/bin/bash

# This script should be executed only by GitLab CI/CD

DOCKER_TAG=$1

mvn clean install -DskipTests

MVN_VERSION=$(mvn -q \
 -Dexec.executable=echo \
 -Dexec.args='${project.version}' \
 --non-recursive \
 exec:exec)

GIT_SHORT_SHA=$(git rev-parse --short HEAD)

MVN_DOCKER_TAG="${MVN_VERSION}-${GIT_SHORT_SHA}"
echo -e "\nMVN_DOCKER_TAG ${MVN_DOCKER_TAG}"

docker login -u gitlab-ci-token -p "${CI_BUILD_TOKEN}" registry.gitlab.com

LOCAL_IMAGE="hello-world:${MVN_DOCKER_TAG}"
REMOTE_IMAGE="${DOCKER_REGISTRY_PATH}/hello-world:${DOCKER_TAG}"

echo -e "\nTag and push ${LOCAL_IMAGE} to ${REMOTE_IMAGE}"
docker tag "${LOCAL_IMAGE}" "${REMOTE_IMAGE}"

docker push "${REMOTE_IMAGE}"
```

In this script we pass the DOCKER_TAG variable as start parameter and make a clean install. The Maven version and the unique Git commit short sha  are taken and used to generate MVN_DOCKER_TAG.
After login in the docker registry, we generate the local and remote image names. The local one is created by the settings in our .pom files. We will be able to push an image only if the local and remote names are equal.
Any error in the tag, version or any other variable name will give some nice red messages in the GitLab console logs like:
```
Error parsing reference: "main_code_app-ffc8fbfb/hello-world:" is not a valid repository/tag: invalid reference format
Error response from daemon: No such image: hello-world:main_code_app-ffc8fbfb
```
or:
```
 Tag and push hello-world:0.0.1-SNAPSHOT-ce03a09 to registry.gitlab.com/datastork-examples/gitlab-docker-setup-demo-ci/hello-world:main_code_app-ce03a099
 Error response from daemon: No such image: hello-world:0.0.1-SNAPSHOT-ce03a09
 The push refers to repository [registry.gitlab.com/datastork-examples/gitlab-docker-setup-demo-ci/hello-world]
 An image does not exist locally with the tag: registry.gitlab.com/datastork-examples/gitlab-docker-setup-demo-ci/hello-world
```
Our pipelines could be seen here:
https://gitlab.com/datastork-examples/gitlab-docker-setup-demo-ci/pipelines
![Alt text](images/pipelines.jpg?raw=true "pipelines")

The first 3 jobs are auto executed on every commit. Docker build-image is manual, so we need to start it:
![Alt text](images/selections.jpg?raw=true "selections")

The last steps  in ``utils/ci-cd/push-docker-images.sh`` are to tag our local image with the remote name. Push command pushes it to the registry. The result on success could be found here
https://gitlab.com/datastork-examples/gitlab-docker-setup-demo-ci/container_registry

In the next part we will explain the natural extension of GitLab Continuous Integration - Continuous Deployment (GitLab CI/CD)