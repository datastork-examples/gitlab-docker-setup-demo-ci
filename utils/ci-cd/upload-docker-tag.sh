#!/bin/bash
set -x

# This script should be exeucted only by GitLab CI/CD

USER_EMAIL=${1}
DOCKER_TAG=${2}
SERVER_USER_HOST=${3}
SERVER_SSH_PORT=${4}
SERVER_SSH_KEY_PATH=${5}

if [ -z "${USER_EMAIL}" ]; then
  echo "Please specify valid USER_EMAIL" 1>&2
  exit 1
fi

if [ -z "${DOCKER_TAG}" ]; then
  echo "Please specify valid DOCKER_TAG" 1>&2
  exit 1
fi

if [ -z "${SERVER_USER_HOST}" ]; then
  echo "Please specify valid SERVER_USER_HOST" 1>&2
  exit 2
fi

re='^[0-9]+$'
if ! [[ ${SERVER_SSH_PORT} =~ $re ]]; then
  echo "Please specify valid SERVER_SSH_PORT" 1>&2
  exit 2
fi

if [ ! -f "${SERVER_SSH_KEY_PATH}" ]; then
  echo "Please specify valid SERVER_SSH_KEY_PATH" 1>&2
  exit 2
fi

REMOTE_RELEASE_DIR='~/releases/backend'

SSH_OPT="-o ConnectTimeout=10 -o LogLevel=quiet \
 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

chmod 600 "${SERVER_SSH_KEY_PATH}"

function execute_ssh_cmd() {
  ssh -v -i "${SERVER_SSH_KEY_PATH}" -p "${SERVER_SSH_PORT}" \
    ${SSH_OPT} "${SERVER_USER_HOST}" "${1}"
}

set -e

echo -e "\nUpload DOCKER_TAG: ${DOCKER_TAG} to ${SERVER_USER_HOST}:${REMOTE_RELEASE_DIR}/"
execute_ssh_cmd "rm -f ${REMOTE_RELEASE_DIR}"'/*'
execute_ssh_cmd "echo '${DOCKER_TAG}' > ${REMOTE_RELEASE_DIR}/docker-tag"
execute_ssh_cmd "echo '${USER_EMAIL}' > ${REMOTE_RELEASE_DIR}/executor-user-email"

execute_ssh_cmd "ls -al ${REMOTE_RELEASE_DIR}/"

execute_ssh_cmd "cat ${REMOTE_RELEASE_DIR}/docker-tag"
