#!/bin/bash

# This script should be exeucted only by GitLab CI/CD

DOCKER_TAG=$1

mvn clean install -DskipTests

MVN_VERSION=$(mvn -q \
  -Dexec.executable=echo \
  -Dexec.args='${project.version}' \
  --non-recursive \
  exec:exec)

GIT_SHORT_SHA=$(git rev-parse --short HEAD)

MVN_DOCKER_TAG="${MVN_VERSION}-${GIT_SHORT_SHA}"
echo -e "\nMVN_DOCKER_TAG ${MVN_DOCKER_TAG}"

docker login -u gitlab-ci-token -p "${CI_BUILD_TOKEN}" registry.gitlab.com

LOCAL_IMAGE="hello-world:${MVN_DOCKER_TAG}"
REMOTE_IMAGE="${DOCKER_REGISTRY_PATH}/hello-world:${DOCKER_TAG}"

echo -e "\nTag and push ${LOCAL_IMAGE} to ${REMOTE_IMAGE}"
docker tag "${LOCAL_IMAGE}" "${REMOTE_IMAGE}"

docker push "${REMOTE_IMAGE}"
``
