#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

GIT_BASE="${DIR}/../.."

# Exit on any command failure
set -e

"${GIT_BASE}/docker-compose/docker-compose.sh" down

"${GIT_BASE}/docker-compose/docker-compose.sh" up -d

HEALTH=""

function getHelth {
  HEALTH=$(curl -s http://localhost:18080/actuator/health | jq -r '.status')
}

echo "Waiting REST API to be UP..."

for i in {1..10}; do
  getHelth
  if [[ "${HEALTH}" = "UP" ]]; then
    echo "REST API is UP"
    exit 0
  fi
  sleep 5
done

echo "Failed to check for UP health status"
exit 1
