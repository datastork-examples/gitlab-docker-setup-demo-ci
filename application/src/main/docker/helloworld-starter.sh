#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar "${DIR}/app.jar"
