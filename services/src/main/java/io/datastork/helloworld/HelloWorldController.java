package io.datastork.helloworld;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Api("Hello world app example")
@RestController
public class HelloWorldController {

  @ApiOperation(value = "Send Hello World",
      notes = "default endpoint")
  @GetMapping(path = "/hello-world")
  public String sayHelloWorld() {
    return "Hello World";
  }

  @ApiOperation(value = "Greet a name")
  @GetMapping(path = "/hello-world/{name}")
  public HelloWorldModel helloWorldPathVariable(@PathVariable final String name) {
    final HelloWorldModel result = new HelloWorldModel(String.format("Hello World, %s", name));
    return result;
  }
}