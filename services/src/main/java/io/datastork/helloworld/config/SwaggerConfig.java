package io.datastork.helloworld.config;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Value("${documentation.services.name}")
  private String apiName;

  @Value("${documentation.services.version}")
  private String apiVersion;

  @Value("${documentation.services.description}")
  private String apiDescription;

  @Value("${documentation.services.host:#{null}}")
  private String apiHost;

  @Value("${documentation.services.basePath:#{null}}")
  private String apiBasePath;

  @Value("${documentation.services.contact.url}")
  private String contactUrl;

  @Value("${documentation.services.contact.email}")
  private String contactEmail;

  static final String HEADER_SECURITY_TOKEN = "Authorization";

  @Bean
  public Docket api(final ServletContext servletContext) {
    final RelativePathProvider relativePathProvider;
    if (apiBasePath != null) {
      relativePathProvider = new RelativePathProvider(servletContext) {
        @Override
        public String getApplicationBasePath() {
          return apiBasePath;
        }
      };
    } else {
      relativePathProvider = new RelativePathProvider(servletContext);
    }

    return (new Docket(DocumentationType.SWAGGER_2))
        .host(apiHost)
        .pathProvider(relativePathProvider)
        .apiInfo(apiInfo())
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.any())
        .build()
        .securitySchemes(Lists.newArrayList(apiKey()))
        .securityContexts(Lists.newArrayList(securityContext()));
  }


  private ApiInfo apiInfo() {
    final Contact contact = new Contact("Info",
        contactUrl,
        contactEmail);
    return new ApiInfo(apiName, apiDescription, apiVersion,
        null, contact,
        null, null, new ArrayList<>());
  }

  private ApiKey apiKey() {
    return new ApiKey(
        HEADER_SECURITY_TOKEN,
        HEADER_SECURITY_TOKEN,
        ApiKeyVehicle.HEADER.getValue());
  }

  private SecurityContext securityContext() {
    return SecurityContext.builder()
        .securityReferences(defaultAuth()).forPaths(PathSelectors.any()).build();
  }

  private List<SecurityReference> defaultAuth() {
    final AuthorizationScope authorizationScope =
        new AuthorizationScope("global", "accessEverything");
    final AuthorizationScope[] authorizationScopes = new AuthorizationScope[]{authorizationScope};
    return Lists.newArrayList(new SecurityReference(HEADER_SECURITY_TOKEN, authorizationScopes));
  }

}