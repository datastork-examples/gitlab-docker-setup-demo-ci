package io.datastork.helloworld;

public class HelloWorldModel {

  private String message;

  public HelloWorldModel() {
  }

  public HelloWorldModel(final String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(final String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return String.format("HelloWorldObject %s", message);
  }
}