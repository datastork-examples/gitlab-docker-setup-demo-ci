package io.datastork.helloworld.rest;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.client.MockMvcClientHttpRequestFactory;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

public class BaseTest {

  @Autowired
  protected MockMvc mvc;

  protected RestTemplate restTemplate;

  @Before
  public void setUp() {

    final MockMvcClientHttpRequestFactory requestFactory =
        new MockMvcClientHttpRequestFactory(mvc);

    restTemplate = new RestTemplate(requestFactory);
  }
}
