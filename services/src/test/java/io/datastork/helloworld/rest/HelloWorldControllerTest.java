package io.datastork.helloworld.rest;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.HttpStatus.OK;

import io.datastork.helloworld.HelloWorldModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
public class HelloWorldControllerTest extends BaseTest {

  @Test
  public void helloWorldPathVariable() {

    final String testString = "some short string";

    final ResponseEntity<HelloWorldModel> responseEntity =
        restTemplate.getForEntity(
            "/hello-world/" + testString,
            HelloWorldModel.class);
    assertEquals(OK, responseEntity.getStatusCode());
    assertNotNull(responseEntity.getBody());
    assertTrue(responseEntity.getBody().getMessage()
        .contains(String.format("Hello World, %s", testString)));
  }
}